org-roam (2.2.2+git20250313.db4170a-1) unstable; urgency=medium

  * Update to new upstream version 2.2.2+git20250313.db4170a.
  * Use d/s/options to avoid extraneous diffs
    - Drop save/restore handling in d/rules in favor of d/s/options
    - Add d/clean to clean up generated files during build

 -- Xiyue Deng <manphiz@gmail.com>  Sat, 15 Mar 2025 23:24:16 -0700

org-roam (2.2.2+git20250218.0037daa-1) unstable; urgency=medium

  [ Xiyue Deng ]
  * Update to new upstream version 2.2.2+git20250218.0037daa.
  * Add d/salsa-ci.yml with default template
  * Add git <!nocheck> to Build-Depends
    - This is required when ERT is run on Salsa CI
  * Ensure dh_elpa_test is run in UTC timezone
    - This fixes Salsa CI reprotest which tries to run the tests under a
      different timezone to check for diff.
  * Drop 0005-Set-org-roam-directory-to-a-non-existent-path-to-ens.patch;
    applied upstream
  * Save/Restore/Cleanup files changed during build to fix
    build-twice-in-a-row

 -- Xiyue Deng <manphiz@gmail.com>  Wed, 19 Feb 2025 00:55:52 -0800

org-roam (2.2.2+git20250111.425d53d-1) unstable; urgency=medium

  * Update to new upstream snapshot 2.2.2+git20250111.425d53d (Closes:
    #1048517, #1091945)
  * Update d/watch using substitute strings to be more robust
  * Set upstream metadata fields: Bug-Database, Bug-Submit,
    Repository-Browse.
  * Update Standards-Version to 4.7.0; no change needed
  * Set section of org-roam-doc to doc (Closes: #1076359)
  * Add d/org-roam-doc.doc-base for doc-base registration
  * Drop oldoldstable emacs version from Recommends
  * Add myself to Uploaders
  * Add d/gbp.conf matching the current practice
  * Add d/README.source documenting the dgit-maint-debrebase workflow
  * Use regexp match to replace hard-coded path equal test
  * Comment out flaky check in org-roam-file-p test
  * Set org-roam-directory to a non-existent path to ensure robust test

 -- Xiyue Deng <manphiz@gmail.com>  Sat, 11 Jan 2025 15:53:19 -0800

org-roam (2.2.2-4) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 23:12:12 +0900

org-roam (2.2.2-3) unstable; urgency=medium

  * Restore rebuilding org-roam.texi (Closes: #1076116).

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Thu, 11 Jul 2024 16:06:23 +0200

org-roam (2.2.2-2) unstable; urgency=medium

  * Adopt the package from Sean Whitton.
  * Bump standards version to 4.6.2 (no changes required).
  * Stop rebuilding texi documentation, use upstream provided version
    (Closes: #1074742).

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Tue, 09 Jul 2024 14:47:42 +0200

org-roam (2.2.2-1) unstable; urgency=medium

  * Update to new upstream version 2.2.2.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 30 Apr 2022 14:40:09 -0700

org-roam (2.2.1-1) unstable; urgency=medium

  * Update to new upstream version 2.2.1.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 10 Apr 2022 11:07:22 -0700

org-roam (2.2.0-1) unstable; urgency=medium

  * Update to new upstream version 2.2.0.
  * Add b-d on elpa-org-contrib for ox-extra.el (Closes: #1002781).
  * Drop reference to makem.sh from d/copyright.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 09 Feb 2022 17:36:50 -0700

org-roam (2.1.0-2) unstable; urgency=medium

  * Also install extensions/*.el.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 24 Aug 2021 14:04:35 -0700

org-roam (2.1.0-1) unstable; urgency=medium

  * Update to new upstream version 2.1.0.
  * Drop obsolete patches.
  * Update patch to doc/Makefile to call package-initialize.
  * Refresh remaining patches.
  * Update d/copyright.
  * Add build-dep on elpa-magit-section.
  * Update build-dep elpa-emacsql-sqlite3 -> elpa-emacsql-sqlite.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 24 Aug 2021 13:14:45 -0700

org-roam (1.2.3-2) unstable; urgency=medium

  * Cherry-pick upstream commit 643b98ee to avoid ignoring the user's
    custom value for org-roam-dailies-directory.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 08 Mar 2021 10:39:04 -0700

org-roam (1.2.3-1) unstable; urgency=medium

  * Update to new upstream version 1.2.3.
  * Update org-roam-doc.docs: upstream has switched the manual to single-page.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 29 Dec 2020 13:01:05 -0700

org-roam (1.2.2-1) unstable; urgency=medium

  * Update to new upstream version 1.2.2.
  * Drop patch to org-roam.el regarding emacsql-sqlite3 dependency.
  * Update d/copyright for new release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 31 Oct 2020 15:02:30 -0700

org-roam (1.2.1-1) unstable; urgency=medium

  * Update to new upstream version 1.2.1.
  * Drop Files-Excluded.
    Upstream has included doc/img/logo.svg in this release.
  * elpa-org-roam Suggests: org-roam-doc.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 27 Jul 2020 14:12:21 -0700

org-roam (1.2.0+dfsg-2) unstable; urgency=medium

  * Source-only upload.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 26 Jul 2020 14:37:36 -0700

org-roam (1.2.0+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #963831).

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 21 Jul 2020 14:37:22 -0700
